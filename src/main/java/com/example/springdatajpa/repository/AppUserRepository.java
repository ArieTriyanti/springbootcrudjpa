package com.example.springdatajpa.repository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springdatajpa.model.AppUser;

import java.util.List;
import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Integer>  {
    AppUser findOneById(int id);
    AppUser findOneByFirstName(String firstName);
    AppUser findOneByLastName(String lastName);
}
